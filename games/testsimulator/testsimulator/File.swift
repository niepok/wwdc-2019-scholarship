//////
//////  SimulatorScene.swift
//////  Book_Sources
//////
//////  Created by Adam Niepokój on 22/03/2019.
//////
////
////import Foundation
////import SpriteKit
////import CoreMotion
////
////
////
////class Plane {
////    let startPosition: (xy: CGPoint, z: CGFloat)
////    let finalPosition  = (xy: CGPoint(x: 0.0, y: 0.0), z: 0.0)
////    let speedX: CGFloat = 150.0 * 0.514444 // 150 knots to m/s
////    var startTime: Date
////
////    var currentPosition: (xy: CGPoint, z: CGFloat) {
////        guard let difference = Calendar.current.dateComponents([Calendar.Component.second], from: startTime, to: Date()).second else { return (xy: .zero, z: 0.0) }
////        return (xy: CGPoint(
////            x: startPosition.xy.x - speedX * CGFloat(difference),
////            y: currentY),
////                z: currentZ)
////    }
////    var distanceToAirport: CGFloat {
////        return currentPosition.xy.x
////    }
////    var height: CGFloat {
////        return currentPosition.z
////    }
////    var inclinationY: CGFloat {
////        return currentPosition.xy.y
////    }
////    var inclinationZ: CGFloat {
////        return currentPosition.z - a * currentPosition.xy.x
////    }
////
////    // set from gyro
////    var angleOfAttack: CGFloat = 0.0
////    var currentZ: CGFloat
////    var currentY: CGFloat
////
////
////    var speedZ: CGFloat {
////        return speedX * tan(angleOfAttack)
////    }
////    init(startPosition: (xy: CGPoint, z: CGFloat)) {
////        self.startPosition = startPosition
////        self.startTime = Date()
////        self.currentZ = startPosition.z
////        self.currentY = startPosition.xy.y
////
////    }
////    var a: CGFloat {
////        return startPosition.z/startPosition.xy.x
////    }
////
////    func toString() -> String {
////        return "plane: x:\(currentPosition.xy.x), y:\(currentPosition.xy.y), z:\(currentPosition.z)"
////    }
////
////}
////
////class SimulatorScene: SKScene {
////
////    private let inclinationSpinnerNode = SKSpriteNode(imageNamed: "sim-inclination-spinner")
////    private let inclinationSpinnerNode2 = SKSpriteNode(imageNamed: "sim-inclination-spinner-two")
////
////    private let inclinationMiddleNode = SKSpriteNode(imageNamed: "sim-middle")
////    private let inclinationBounds = SKSpriteNode(imageNamed: "sim-inclination-bounds")
////
////    private let inclinationSteadyNode = SKSpriteNode(imageNamed: "sim-inclination-steady")
////    private let inclinationSteadyNode2 = SKSpriteNode(imageNamed: "sim-inclination-steady-two")
////
////    private let slopeNode = SKSpriteNode(imageNamed: "sim-slope")
////    private let slopeIndicatorNode = SKSpriteNode(imageNamed: "sim-slope-indicator")
////    private var slopeBounds: SKSpriteNode!
////
////    private let courseNode = SKSpriteNode(imageNamed: "sim-course")
////    private let courseIndicatorNode = SKSpriteNode(imageNamed: "sim-course-indicator")
////    private var courseBounds: SKSpriteNode!
////
////    private let courseDistanceNode = SKSpriteNode(imageNamed: "sim-course-distance")
////    private let courseDistanceIndicator = SKSpriteNode(imageNamed: "sim-course-distance-indicator")
////
////    private var labelNode: SKLabelNode!
////
////    private let papiFrame = SKSpriteNode(imageNamed: "sim-papi-frame")
////    private let papiNodes = [
////        PapiNode(imageNamed: "sim-papi-white"),
////        PapiNode(imageNamed: "sim-papi-white"),
////        PapiNode(imageNamed: "sim-papi-white"),
////        PapiNode(imageNamed: "sim-papi-white")
////    ]
////
////    private class PapiNode: SKSpriteNode {
////        let redTexture = SKTexture(imageNamed: "sim-papi-red")
////        let whiteTexture = SKTexture(imageNamed: "sim-papi-white")
////
////        var state = true {
////            didSet {
////                self.texture = state ? whiteTexture : redTexture
////            }
////        }
////    }
////
////    private var motionManager = CMMotionManager()
////
////    private var plane: Plane!
////    public var startPosition = (xy: CGPoint(x:8000.0,y:0.0), z: CGFloat(500.0))
////
////    override func didMove(to view: SKView) {
////        scene?.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
////        addNodesToScene()
////        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
////        physicsWorld.contactDelegate = self
////        //plane = Plane(startPosition: self.startPosition)
////        startQueuedUpdates()
////    }
////
////    //    TODO: Important
////    //    If your app relies on the presence of accelerometer hardware, configure
////    //    the UIRequiredDeviceCapabilities key of its Info.plist file with the accelerometer value. For more information about the meaning of this key, see Information Property List Key Reference.
////
////    override func willMove(from view: SKView) {
////        motionManager.stopDeviceMotionUpdates()
////    }
////
////    private let middle = CGPoint(x: 0.5, y: 0.5)
////
////    private func addInclination() {
////        inclinationSteadyNode.anchorPoint = middle
////        addChild(inclinationSteadyNode)
////
////        inclinationMiddleNode.anchorPoint = middle
////        inclinationMiddleNode.zPosition = 1
////        inclinationSteadyNode.addChild(inclinationMiddleNode)
////
////        inclinationSteadyNode2.anchorPoint = middle
////        inclinationSteadyNode2.zPosition = 3
////        inclinationSteadyNode.addChild(inclinationSteadyNode2)
////
////        inclinationSpinnerNode2.anchorPoint = middle
////        inclinationSpinnerNode2.zPosition = 1
////        inclinationMiddleNode.addChild(inclinationSpinnerNode2)
////
////        inclinationBounds.anchorPoint = middle
////        inclinationBounds.zPosition = -2
////        inclinationSteadyNode2.addChild(inclinationBounds)
////
////        inclinationMiddleNode.physicsBody = SKPhysicsBody(texture: inclinationMiddleNode.texture!, size: inclinationMiddleNode.size)
////        setBoundsProperties(for: inclinationMiddleNode.physicsBody)
////
////        inclinationBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: inclinationBounds.frame)
////        setBoundsProperties(for: inclinationBounds.physicsBody)
////    }
////
////    private func setBoundsProperties(for physicsBody: SKPhysicsBody?) {
////        guard let body = physicsBody else { return }
////        body.friction = 0
////        body.restitution = 0
////        body.linearDamping = 0
////        body.angularDamping = 0
////        body.allowsRotation = false
////    }
////
////    private func addSlope() {
////        slopeNode.anchorPoint = middle
////        slopeNode.zPosition = 4
////        slopeNode.position = CGPoint(
////            x: 900,
////            y: 0)
////        inclinationSteadyNode.addChild(slopeNode)
////
////        slopeIndicatorNode.anchorPoint = middle
////        slopeIndicatorNode.zPosition = 2
////        slopeNode.addChild(slopeIndicatorNode)
////
////        slopeBounds = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), size: slopeNode.size)
////        slopeBounds.anchorPoint = middle
////        slopeBounds.zPosition = 2
////        slopeNode.addChild(slopeBounds)
////        slopeBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: slopeBounds.frame)
////        setBoundsProperties(for: slopeBounds.physicsBody)
////
////        slopeIndicatorNode.physicsBody = SKPhysicsBody(texture: slopeIndicatorNode.texture!, size: slopeIndicatorNode.size)
////        setBoundsProperties(for: slopeIndicatorNode.physicsBody)
////    }
////
////    private func addCourse() {
////        courseNode.anchorPoint = middle
////        courseNode.zPosition = 4
////        courseNode.position = CGPoint(
////            x: 0,
////            y: -1000)
////        inclinationSteadyNode.addChild(courseNode)
////
////        courseIndicatorNode.anchorPoint = middle
////        courseIndicatorNode.zPosition = 2
////        courseNode.addChild(courseIndicatorNode)
////
////        courseBounds = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), size: courseNode.size)
////        courseBounds.anchorPoint = middle
////        courseBounds.zPosition = 2
////        slopeNode.addChild(courseBounds)
////        courseBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: slopeBounds.frame)
////        setBoundsProperties(for: courseBounds.physicsBody)
////
////        courseIndicatorNode.physicsBody = SKPhysicsBody(texture: courseIndicatorNode.texture!, size: courseIndicatorNode.size)
////        setBoundsProperties(for: courseIndicatorNode.physicsBody)
////
////    }
////
////    private func addPapis() {
////        papiFrame.anchorPoint = middle
////        papiFrame.zPosition = 4
////        papiFrame.position = CGPoint(
////            x: 0,
////            y: 1100)
////        inclinationSteadyNode.addChild(papiFrame)
////
////        var i = 0
////        for papi in papiNodes {
////            papi.anchorPoint = middle
////            let part = CGFloat(-3+2*i)
////            papi.position = CGPoint(
////                x: part*papiFrame.size.width/8,
////                y: 0)
////            papi.zPosition = 1
////            papiFrame.addChild(papi)
////            i += 1
////        }
////    }
////
////    private func addNodesToScene() {
////        addInclination()
////        addSlope()
////        addCourse()
////        addPapis()
////        //addLabel()
////    }
////
////    private func addLabel() {
////        labelNode = SKLabelNode(fontNamed: UIFont.systemFont(ofSize: 10.0).fontName)
////        labelNode.zPosition = 2
////        labelNode.position = CGPoint(x: papiFrame.frame.width/2, y: papiFrame.frame.height/2)
////        labelNode.numberOfLines = 5
////        labelNode.fontSize = 10.0
////        labelNode.text = "test"
////        papiFrame.addChild(labelNode)
////
////    }
////
////    override func didChangeSize(_ oldSize: CGSize) {
////        super.didChangeSize(oldSize)
////        print("from \(previousOrientation.rawValue) on: [roll: \(lastRoll) pitch: \(lastPitch) yaw: \(lastYaw)] to \(UIDevice.current.orientation.rawValue)")
////        inclinationSteadyNode.scale(to: CGSize(
////            width: size.height/2*inclinationSteadyNode.size.width/inclinationSteadyNode.size.height,
////            height: size.height/2))
////        previousOrientation = UIDevice.current.orientation
////    }
////
////    public var previousOrientation = UIDeviceOrientation.unknown
////
////    override func update(_ currentTime: TimeInterval) {}
////
////    private var lastRoll: Double?
////    private var lastPitch: Double?
////    private var lastYaw: Double?
////
////}
////
////
////// Motion Handling
////extension SimulatorScene {
////    private func startQueuedUpdates() {
////        if motionManager.isDeviceMotionAvailable {
////            self.motionManager.deviceMotionUpdateInterval = 1.0 / 30.0
////            self.motionManager.showsDeviceMovementDisplay = true
////            self.motionManager.startDeviceMotionUpdates(
////                using: .xArbitraryZVertical,
////                to: .main, withHandler: { (data, error) in
////                    if let validData = data {
////                        let roll = validData.attitude.roll
////                        let pitch = validData.attitude.pitch
////                        let yaw = validData.attitude.yaw
////                        self.lastRoll = roll
////                        self.lastYaw = yaw
////                        self.lastPitch = pitch
////                        //self.handleMotionData(roll, pitch, yaw)
////                    }
////            })
////        }
////    }
////
////    private func handleMotionData(_ roll: Double, _ pitch: Double, _ yaw: Double) {
////        var difr, difp, dify: Double
////        if let lr = lastRoll, let lp = lastPitch, let ly = lastYaw {
////            difr = roll/lr
////            difp = pitch/lp
////            dify = yaw/ly
////        } else {
////            difr = roll
////            difp = pitch
////            dify = yaw
////        }
////        //        lastRoll = roll
////        //        lastYaw = yaw
////        //        lastPitch = pitch
////
////        switch UIDevice.current.orientation {
////        case .portrait:
////            self.inclinationMiddleNode.position.y = self.inclinationMiddleNode.position.y * CGFloat(-difp) // CGFloat(-pitch+Double.pi/4)*size.height/6
////            self.inclinationMiddleNode.zRotation = CGFloat(roll)
////        case .portraitUpsideDown:
////            self.inclinationMiddleNode.position.y = self.inclinationMiddleNode.position.y * CGFloat(difp)
////        // CGFloat(-pitch+Double.pi/4)*size.height/6            self.inclinationMiddleNode.zRotation = CGFloat(-roll)
////        case .landscapeLeft:
////            self.inclinationMiddleNode.position.y = CGFloat(roll+Double.pi/4)*size.height/6
////            self.inclinationMiddleNode.zRotation = CGFloat(pitch)
////        case .landscapeRight:
////            self.inclinationMiddleNode.position.y = CGFloat(-roll+Double.pi/4)*size.height/6
////            self.inclinationMiddleNode.zRotation = CGFloat(-pitch)
////            ////        case .faceUp:
////            //            self.inclinationMiddleNode.zRotation = CGFloat(-yaw)
////            //            switch previousOrientation {
////            //            case .portrait:
////            //                self.inclinationMiddleNode.position.y = CGFloat(-pitch+Double.pi/4)*size.height/6 + size.height/6
////            //            case .portraitUpsideDown:
////            //                self.inclinationMiddleNode.position.y = CGFloat(pitch+Double.pi/4)*size.height/6 + size.height/6
////            //            case .landscapeLeft:
////            //                self.inclinationMiddleNode.position.y = CGFloat(roll+Double.pi/4)*size.height/6 + size.height/6
////            //            case .landscapeRight:
////            //                self.inclinationMiddleNode.position.y = CGFloat(-roll+Double.pi/4)*size.height/6 + size.height/6
////            //            default:
////            //                self.inclinationMiddleNode.position.y = CGFloat(-roll+Double.pi/4)*size.height/6 + size.height/6
////            //            }
////            //        case .faceDown:
////            //            self.inclinationMiddleNode.zRotation = CGFloat(yaw)
////            //            switch previousOrientation {
////            //            case .portrait:
////            //                self.inclinationMiddleNode.position.y = CGFloat(-pitch+Double.pi/4)*size.height/6 + size.height/6
////            //            case .portraitUpsideDown:
////            //                self.inclinationMiddleNode.position.y = CGFloat(pitch+Double.pi/4)*size.height/6 + size.height/6
////            //            case .landscapeLeft:
////            //                self.inclinationMiddleNode.position.y = CGFloat(roll+Double.pi/4)*size.height/6 + size.height/6
////            //            case .landscapeRight:
////            //                self.inclinationMiddleNode.position.y = CGFloat(-roll+Double.pi/4)*size.height/6 + size.height/6
////            //            default:
////            //                self.inclinationMiddleNode.position.y = CGFloat(roll+Double.pi/4)*size.height/6 + size.height/6
////        //            }
////        default:
////            return
////        }
////        print("roll: \(roll) pitch: \(pitch) yaw: \(yaw) position.y \(self.inclinationMiddleNode.position.y) zRotation: \(self.inclinationMiddleNode.zRotation) ")
////        //        print(self.inclinationMiddleNode.position.y)
////        //updatePlane()
////    }
////
////    // TODO: start from one of the sides!
////    private func updatePlane() {
////        // inclinationMiddleNode z rotation -> plane y
////        // inclinationMiddleNode position y -> angle of attack + plane z
////        plane.currentY = plane.currentY + plane.speedX / (CGFloat(self.motionManager.deviceMotionUpdateInterval) * tan(CGFloat.pi/2-inclinationMiddleNode.zRotation))
////        plane.angleOfAttack = -inclinationMiddleNode.position.y / 20.0
////        plane.currentZ = plane.currentZ - plane.speedZ / (CGFloat(self.motionManager.deviceMotionUpdateInterval))
////        print(plane.toString())
////    }
////}
////
////extension SimulatorScene: SKPhysicsContactDelegate {
////
////}
//
//2019-03-23 11:05:54.613608+0100 testsimulator[1008:107462] [DYMTLInitPlatform] platform initialization successful
//2019-03-23 11:05:54.717456+0100 testsimulator[1008:107411] Metal GPU Frame Capture Enabled
//2019-03-23 11:05:54.718707+0100 testsimulator[1008:107411] Metal API Validation Enabled
//from 0 on: [roll: nil pitch: nil yaw: nil] to 0
//from 0 on: [roll: nil pitch: nil yaw: nil] to 0
//from 0 on: [roll: nil pitch: nil yaw: nil] to 1
//from 1 on: [roll: Optional(1.5108947337821348) pitch: Optional(-0.014019183155804565) yaw: Optional(-0.0078110008993106635)] to 4
//from 4 on: [roll: Optional(1.5108947337821348) pitch: Optional(-0.014019183155804565) yaw: Optional(-0.0078110008993106635)] to 4
//from 4 on: [roll: Optional(1.2853735748830746) pitch: Optional(-1.325660797643035) yaw: Optional(-0.15728199921194524)] to 2
//from 2 on: [roll: Optional(1.2853735748830746) pitch: Optional(-1.325660797643035) yaw: Optional(-0.15728199921194524)] to 2
//from 2 on: [roll: Optional(-1.5730198914898217) pitch: Optional(-0.2939915457289561) yaw: Optional(-3.123998586089099)] to 3
//from 3 on: [roll: Optional(-1.5730198914898217) pitch: Optional(-0.2939915457289561) yaw: Optional(-3.123998586089099)] to 3
//from 3 on: [roll: Optional(-1.4494599440837286) pitch: Optional(1.0919511486903781) yaw: Optional(2.9857296846444488)] to 1
//from 1 on: [roll: Optional(-1.4494599440837286) pitch: Optional(1.0919511486903781) yaw: Optional(2.9857296846444488)] to 1
//from 1 on: [roll: Optional(-1.4147685564589452) pitch: Optional(-0.020662837826592555) yaw: Optional(3.1012367392462594)] to 3
//from 3 on: [roll: Optional(-1.4147685564589452) pitch: Optional(-0.020662837826592555) yaw: Optional(3.1012367392462594)] to 3

//------
//76.8
//-595.75341796875
//-594.9703979492188
//7.55

//------
//76.8
//582.8048095703125
//584.0404663085938
//------

//------
//102.4
//640.8406982421875
//640.7940673828125
