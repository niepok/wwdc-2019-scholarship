//
//  See LICENSE folder for this template’s licensing information.
//
//  Abstract:
//  Provides supporting functions for setting up a live view.
//

import UIKit
import Foundation
import SpriteKit


//TODO: launch scene from receive
public class ViewController: UIViewController {

//    public override func receive(_ message: PlaygroundValue) {
//        guard case .data(let arrayData) = message else { return }
//        do {
//            if let stringArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(arrayData) as? String {
//                loadScene()
//            }
//        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }
//
//    }

    private func loadScene() {
        if let view = self.view as! SKView? {
            let scene = SimulatorScene()
            scene.size = self.view.bounds.size
            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            view.showsPhysics = true
        }
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
//        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        loadScene()
    }

//    @objc func rotated() {
//        print("rotatedTo: \(self.view.bounds.size)")
//        print(UIDevice.current.orientation.rawValue)
////        if let view = self.view as? SKView,
////            let scene = view.scene as? SimulatorScene {
////            scene.isRotating = true
////            scene.size = self.view.bounds.size
////        }
//    }

    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let view = self.view as? SKView,
            let scene = view.scene as? SimulatorScene {
            scene.size = size
            print("viewWillTransitionToSize: \(size)")
            print(UIDevice.current.orientation.rawValue)
        } else {
            view.frame.size = size
        }

    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let view = self.view as? SKView {
            view.scene?.size = self.view.bounds.size
        } else {
            view.frame.size = self.view.bounds.size
        }
    }

    override public var shouldAutorotate: Bool {
        return false
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override public var prefersStatusBarHidden: Bool {
        return true
    }
}
