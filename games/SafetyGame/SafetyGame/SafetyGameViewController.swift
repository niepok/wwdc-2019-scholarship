//
//  GameViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 19/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import SpriteKit

class SafetyGameViewController: UIViewController {

    private var scoreLabel: UILabel!
    private var resultsTableView: UITableView!
    private var cellId = "resultCell"
    private var resultsFromScene = [(image: String, type: Int, typeChosen: Int)]()

    override func viewDidLoad() {
        super.viewDidLoad()
        if let view = self.view as! SKView? {
            let scene = SafetyGameScene()
            scene.size = self.view.bounds.size
            scene.scaleMode = .aspectFill
            scene.showResults = {
                self.showResults(results: scene.getSwippedNode(), score: scene.getScore())
            }
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let view = self.view as? SKView {
            view.scene?.size = size
        } else {
            view.frame.size = size
        }

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let view = self.view as? SKView {
            view.scene?.size = self.view.bounds.size
        } else {
            view.frame.size = self.view.bounds.size
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        if let tableVieww = self.resultsTableView {
            tableVieww.reloadData()
        }
    }

    func showResults(results: [(image: String, type: Int, typeChosen: Int)], score: Int) {
        resultsFromScene = results
        replaceSceneView()
        addScoreLabel(for: score)
        addResultsTableView()
        setAutoLayout()
    }

    private func replaceSceneView() {
        self.view = UIView(frame: self.view.frame)
        self.view.backgroundColor = UIColor.white
    }

    private func addScoreLabel(for score: Int) {
        scoreLabel = UILabel()
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        scoreLabel.text = "Your score: \(score)"
        scoreLabel.textAlignment = .center
        scoreLabel.font = UIFont.boldSystemFont(ofSize: 30.0)
        view.addSubview(scoreLabel)
    }

    private func setAutoLayout() {
        NSLayoutConstraint.activate([
            scoreLabel.topAnchor.constraint(equalTo: view.topAnchor),
            scoreLabel.leftAnchor.constraint(equalTo: view.leftAnchor),
            scoreLabel.rightAnchor.constraint(equalTo: view.rightAnchor),
            scoreLabel.heightAnchor.constraint(equalToConstant: 150),

            resultsTableView.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor),
            resultsTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            resultsTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            resultsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
    }

    private func addResultsTableView() {
        resultsTableView = UITableView()
        resultsTableView.translatesAutoresizingMaskIntoConstraints = false
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
        resultsTableView.register(SafetyGameTableCell.self, forCellReuseIdentifier: cellId)
        view.addSubview(resultsTableView)
    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension SafetyGameViewController: UITableViewDataSource, UITableViewDelegate {

    private class SafetyGameTableCell: UITableViewCell {
        var safetyImageView: UIImageView!
        var originalTypeLabel: UILabel!
        var chosenTypeLabel: UILabel!

        override func prepareForReuse() {
            super.prepareForReuse()
            safetyImageView.image = nil
            originalTypeLabel.text = nil
            chosenTypeLabel.text = nil
        }

        func setImageView(with imageName: String) {
            let image = UIImage(named: imageName)
            safetyImageView = UIImageView()
            safetyImageView.image = image
            safetyImageView.contentMode = .scaleAspectFit
            safetyImageView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(safetyImageView)
        }

        private func getChoiceString(for type: Int) -> String {
            if type == 1 {
                return "'Forbidden'"
            } else if type == 2 {
                return "'Cabin baggage'"
            } else if type == 3 {
                return "'Registered baggage'"
            } else {
                return "Prepare for 'Safety check-in'"
            }
        }

        func setLabels(originalType: Int, chosenType: Int) {
            let chosenText = "You chose " + self.getChoiceString(for: chosenType)
            var originalText = ""
            if originalType == chosenType {
                self.backgroundColor = #colorLiteral(red: 0.8321695924, green: 0.985483706, blue: 0.4733308554, alpha: 0.5)
                originalText =  "✅ Good job!"
            } else {
                self.backgroundColor = #colorLiteral(red: 1, green: 0.4932718873, blue: 0.4739984274, alpha: 0.5)
                originalText = "🚫 You should have chosen " + getChoiceString(for: originalType)
            }
            chosenTypeLabel = UILabel()
            chosenTypeLabel.numberOfLines = 3
            chosenTypeLabel.text = chosenText
            chosenTypeLabel.textAlignment = .center
            chosenTypeLabel.font = chosenTypeLabel.font.withSize(20.0)
            chosenTypeLabel.lineBreakMode = .byWordWrapping
            chosenTypeLabel.minimumScaleFactor = 2.0
            chosenTypeLabel.adjustsFontSizeToFitWidth = true
            chosenTypeLabel.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(chosenTypeLabel)

            originalTypeLabel = UILabel()
            originalTypeLabel.numberOfLines = 3
            originalTypeLabel.text = originalText
            originalTypeLabel.textAlignment = .center
            originalTypeLabel.font = chosenTypeLabel.font.withSize(20.0)
            originalTypeLabel.lineBreakMode = .byWordWrapping
            originalTypeLabel.minimumScaleFactor = 2.0
            originalTypeLabel.adjustsFontSizeToFitWidth = true
            originalTypeLabel.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(originalTypeLabel)
        }

        // TODO: middle label width
        func setAutoLayout() {
            NSLayoutConstraint.activate([
                safetyImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                safetyImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                safetyImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
                //safetyImageView.heightAnchor.constraint(equalToConstant: 130),
                safetyImageView.widthAnchor.constraint(equalToConstant: 130),

                chosenTypeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                chosenTypeLabel.leftAnchor.constraint(equalTo: safetyImageView.rightAnchor, constant: 30),
                chosenTypeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
                //chosenTypeLabel.heightAnchor.constraint(equalToConstant: 130),

                originalTypeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                originalTypeLabel.leftAnchor.constraint(equalTo: chosenTypeLabel.rightAnchor, constant: 30),
                originalTypeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
                originalTypeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
                //originalTypeLabel.heightAnchor.constraint(equalToConstant: 130),

                ])
        }


        override func awakeFromNib() {
            super.awakeFromNib()

            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultsFromScene.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt: IndexPath) -> CGFloat {
        return 150.0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SafetyGameTableCell
        cell.setImageView(with: resultsFromScene[indexPath.row].image)
        cell.setLabels(originalType: resultsFromScene[indexPath.row].type, chosenType: resultsFromScene[indexPath.row].typeChosen)
        cell.setAutoLayout()
        return cell
    }
}
