//
//  GameScene.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 19/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import SpriteKit

class SafetyGameScene: SKScene {
    private var swippedNodes = [(image: String, type: Int, typeChosen: Int)]()
    private var counter = 0
    var userCounter = 1
    private var hasTouchedHole = false
    private var safetyObjectDict = [(String,Int)]()
    private var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    public var showResults: (()->())?

    private var shorterSideLength: CGFloat {
        get {
            if self.size.width < self.size.height {
                return self.size.width
            } else {
                return self.size.height
            }
        }
    }
    
    private var scoreLabelFontSize: CGFloat {
        get {
            return 0.065 * shorterSideLength
        }
    }
    private var pointsAndTranslations: [String:(CGPoint,CGPoint)] {
        get {
            return  ["4":(CGPoint(x: 0, y: 0),CGPoint(x: 0, y: 0)),
                     "2":(CGPoint(x: 0, y: 1),CGPoint(x: 0, y: self.size.height)),
                     "3":(CGPoint(x: 1, y: 0),CGPoint(x: self.size.width, y: 0)),
                     "1":(CGPoint(x: 1, y: 1),CGPoint(x: self.size.width, y: self.size.height))]
        }
    }

    private var currentNode: SKNode?
    private var scoreLabel: SKLabelNode!
    private var dispatchHoles = [SKSpriteNode]()
    private var startButton: UIButton!
    private var currentSafetyItem: SafetyItemNode!


    override func didMove(to view: SKView) {
        self.scene?.backgroundColor = UIColor.gray
        self.scene?.anchorPoint = CGPoint(x: 0.0, y: 0.0)
        loadImages()
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        physicsWorld.contactDelegate = self
        createStartButton()
        placeNodes()
    }

    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        guard oldSize != self.size else { return }

        for hole in dispatchHoles {
            guard let name = hole.name,
                let position = pointsAndTranslations[name]?.1
                else { return }
            hole.scale(to: CGSize(width: shorterSideLength/2, height: shorterSideLength/2))
            hole.position = position
        }

        guard let scoreLabel = self.scoreLabel else { return }
        scoreLabel.fontSize = scoreLabelFontSize
        scoreLabel.position = CGPoint(x: frame.midX, y: frame.maxY - 0.065*shorterSideLength)

        guard let safetyItem = self.currentSafetyItem else {
            guard let startButton = self.startButton else { return }
            startButton.removeFromSuperview()
            createStartButton()
            return
        }
        safetyItem.position = CGPoint(x: self.size.width/2, y: self.size.height/2)
        safetyItem.scaleTo(size: CGSize(width: shorterSideLength/3, height: shorterSideLength/3))



    }

    private func createStartButton() {
        startButton = UIButton(frame: CGRect(
            origin: CGPoint(
                x: self.size.width/2 - shorterSideLength/6,
                y: self.size.height/2 - shorterSideLength/6),
            size: CGSize(width: shorterSideLength/3, height: shorterSideLength/3)))

        startButton.setTitle("Start", for: .normal)
        startButton.addTarget(self, action: #selector(SafetyGameScene.getNextSafetyNode), for: .touchDown)
        startButton.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        startButton.layer.cornerRadius = 20.0
        startButton.setTitleColor(UIColor.black, for: .normal)
        startButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: scoreLabelFontSize)
        self.view?.addSubview(startButton)
    }

    private func createSafetyNode(for index: Int) {
        let nextItem = safetyObjectDict.remove(at: index)
        let itemNode = SafetyItemNode(rectOf: CGSize(width: shorterSideLength/3, height: shorterSideLength/3), cornerRadius: 20.0)
        itemNode.setItemObject(of: nextItem.1, with: nextItem.0)


        itemNode.position = CGPoint(x: frame.midX, y: frame.midY)
        itemNode.physicsBody = SKPhysicsBody(rectangleOf: itemNode.frame.size)

        itemNode.physicsBody!.contactTestBitMask = itemNode.physicsBody!.collisionBitMask
        itemNode.physicsBody?.isDynamic = true
        itemNode.physicsBody?.collisionBitMask = 0

        startButton.removeFromSuperview()
        currentSafetyItem = itemNode
        addChild(itemNode)
        self.shake(node: itemNode, numberOfShakes: 10)
    }


    private func createDispatchHoles() {
        for (key,value) in pointsAndTranslations {
            let hole = SKSpriteNode(imageNamed: key)
            hole.anchorPoint = value.0

            hole.position = value.1
            if self.size.width < self.size.height {
                hole.scale(to: CGSize(width: self.size.width/2, height: self.size.width/2))
            } else {
                hole.scale(to: CGSize(width: self.size.height/2, height: self.size.height/2))
            }

            hole.name = key
            hole.zPosition = -10
            addChild(hole)
            dispatchHoles.append(hole)

            hole.physicsBody = SKPhysicsBody(texture: hole.texture!, size: hole.size)
            hole.physicsBody?.isDynamic = false
        }
    }


    private func createScoreLabel() {
        scoreLabel = SKLabelNode(fontNamed: UIFont.boldSystemFont(ofSize: scoreLabelFontSize).fontName)
        scoreLabel.text = "Score: 0"

        scoreLabel.fontSize = scoreLabelFontSize
        scoreLabel.position = CGPoint(x: frame.midX, y: frame.maxY - 50)

        scoreLabel.fontColor = UIColor.yellow
        addChild(scoreLabel)
    }
}

// collision handling
extension SafetyGameScene: SKPhysicsContactDelegate {
    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "safetyItem" || contact.bodyB.node?.name == "safetyItem" {
            self.currentNode = nil
            if let safetyItem = contact.bodyA.node as? SafetyItemNode {
                if safetyItem.typeName == contact.bodyB.node?.name {
                    score += 1
                } else {
                    score -= 1
                }
                getSwippedNode(for: safetyItem, to: contact.bodyB.node)
            } else if let safetyItem = contact.bodyB.node as? SafetyItemNode {
                if safetyItem.typeName == contact.bodyA.node?.name {
                    score += 1
                } else {
                    score -= 1
                }
                getSwippedNode(for: safetyItem, to: contact.bodyA.node)
            }
            return
        }

        guard contact.bodyA.node != nil && contact.bodyB.node != nil else {
            return
        }
    }

    func didEnd(_ contact: SKPhysicsContact) {
    }
}
// NOT RELATED TO SCENE SIZE
extension SafetyGameScene {

    private class SafetyItemNode: SKShapeNode {
        var type = 0
        var typeName: String {
            return String(type)
        }
        var imageName = ""

        func setItemObject(of type: Int, with imageName: String) {
            self.type = type
            self.fillColor = UIColor.white
            self.name = "safetyItem"
            self.imageName = imageName

            let imageNode = SKSpriteNode(texture: SKTexture(image: UIImage(named: imageName)!))
            imageNode.size = CGSize(width: frame.width/2, height: frame.height/2)
            imageNode.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            imageNode.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
            self.addChild(imageNode)
        }

        func scaleTo(size: CGSize) {
            super.setScale(self.frame.size.width/size.width)
        }
    }

    public func getSwippedNode() -> [(image: String, type: Int, typeChosen: Int)] {
        return swippedNodes
    }

    public func getScore() -> Int {
        return score
    }

    private func placeNodes() {
        createDispatchHoles()
        createScoreLabel()
    }

    private func shake(node: SKNode, numberOfShakes: Float) {
        var shakes = [SKAction]()
        for _ in 1...Int(numberOfShakes) {
            let moveX = Float.random(in: 0...20)/2
            let moveY = Float.random(in: 0...20)/2
            let shake = SKAction.moveBy(x: CGFloat(moveX), y: CGFloat(moveY), duration: 0.01)
            shake.timingMode = .easeOut
            shakes.append(shake)
            shakes.append(shake.reversed())
        }
        let sequence = SKAction.sequence(shakes)
        node.run(sequence)
    }


    // TODO: counter with amount of nodes set but user
    @objc private func getNextSafetyNode() {
        if safetyObjectDict.isEmpty || counter > userCounter {
            endGame()
        } else {
            counter += 1
            let nextIndex = Int.random(in: 0...safetyObjectDict.count-1)
            createSafetyNode(for: nextIndex)
        }
    }

    private func loadImages() {
        var dict = [(String,Int)]()
        let fileManager = FileManager.default
        let imagePath = Bundle.main.resourcePath!
        let items = try! fileManager.contentsOfDirectory(atPath: imagePath)
        for item in items {
            if item.hasPrefix("safety-1-") {
                dict.append((item,1))
            } else if item.hasPrefix("safety-2-") {
                dict.append((item,2))
            } else if item.hasPrefix("safety-3-") {
                dict.append((item,3))
            } else if item.hasPrefix("safety-4-") {
                dict.append((item,4))
            }
        }
        safetyObjectDict = dict
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)

            let touchedNodes = self.nodes(at: location)
            for node in touchedNodes.reversed() {
                if node.name == "safetyItem" {
                    self.currentNode = node
                }
            }
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first, let node = self.currentNode {
            let touchLocation = touch.location(in: self)
            node.position = touchLocation
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.currentNode = nil
    }

    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }

    private func endGame() {
        showResults?()
    }

    private func getSwippedNode(for node: SafetyItemNode, to hole: SKNode?) {
        guard let holeNode = hole,
            let name = holeNode.name,
            let type = Int(name)
            else { return }
        swippedNodes.append((image: node.imageName, type: node.type, typeChosen: type))
        suckSwipped(node: node, to: holeNode)
    }

    private func suckSwipped(node safetyItem: SafetyItemNode, to hole: SKNode) {
        let hide = SKAction.hide()
        let remove = SKAction.run {
            safetyItem.removeFromParent()
        }
        let getNew = SKAction.run {
            self.getNextSafetyNode()
        }
        let removeSequence = SKAction.sequence([hide,remove,getNew])
        safetyItem.run(removeSequence)
    }
}
