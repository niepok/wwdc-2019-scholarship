//
//  GameViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 19/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let view = self.view as! SKView? {
            let scene = SafetyGameScene()
            scene.size = view.frame.size
            scene.scaleMode = .aspectFill
            scene.showResults = {
                self.showResults(results: scene.getSwippedNode(), score: scene.getScore())
            }

            // Present the scene
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            view.showsPhysics = true
        }
    }

    func showResults(results: [(image: String, type: Int, typeChosen: Int)], score: Int) {
        print(results)
        print("Score: \(score)")

    }

    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
}

