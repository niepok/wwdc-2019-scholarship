//
//  SafetyGameViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 19/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//
import UIKit
import Foundation
import PlaygroundSupport
import SpriteKit


public class SafetyGameViewController: LiveViewController {
    
    private var scoreLabel: UILabel!
    private var resultsTableView: UITableView!
    private var cellId = "resultCell"
    private var resultsFromScene = [(image: String, type: Int, typeChosen: Int)]()
    private var sceneView: SKView!

    private func addSceneView() {
        sceneView = SKView()
        sceneView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(sceneView)
        NSLayoutConstraint.activate([
            sceneView.rightAnchor.constraint(equalTo: view.rightAnchor),
            sceneView.leftAnchor.constraint(equalTo: view.leftAnchor),
            sceneView.topAnchor.constraint(equalTo: view.topAnchor),
            sceneView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
    }

    private func loadScene() {
        if let sview = sceneView {
            let scene = SafetyGameScene()
            scene.size = self.view.bounds.size
            scene.scaleMode = .aspectFill
            scene.userCounter = amountOfItems
            scene.showResults = {
                self.showResults(results: scene.getSwippedNode(), score: scene.getScore())
            }
            sview.presentScene(scene)
            sview.ignoresSiblingOrder = true
        }
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        addSceneView()
        loadScene()
    }

    private var amountOfItems = 8

    public override func receive(_ message: PlaygroundValue) {
        guard case .data(let arrayData) = message else { return }
        do {
            if let amountOfItems = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(arrayData) as? Int {
                if sceneView != nil, let scene = sceneView.scene as? SafetyGameScene {
                    scene.userCounter = amountOfItems
                } else {
                    self.amountOfItems = amountOfItems
                    for vieww in self.view.subviews {
                        vieww.removeFromSuperview()
                    }
                    addSceneView()
                    loadScene()
                }
            } else {

            }
        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }

    }


    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let view = sceneView {
            view.scene?.size = size
        } else {
            view.frame.size = size
            //            scoreLabel.text = scoreLabel.text! + " willtransition"

        }
    }
    private var counter = 0

    override public func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let view = sceneView {
            view.scene?.size = self.view.bounds.size
        } else {
            //scoreLabel.text = scoreLabel.text! + " willlayout"
        }
    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let view = sceneView {
            view.scene?.size = self.view.bounds.size
        } else {
            view.frame.size = self.view.bounds.size
            //scoreLabel.text = scoreLabel.text! + " didlayout"
            //view.layoutSubviews()
        }
    }

    func showResults(results: [(image: String, type: Int, typeChosen: Int)], score: Int) {
        resultsFromScene = results
        replaceSceneView()
        addScoreLabel(for: score)
        addResultsTableView()
        setAutoLayout()
    }

    private func replaceSceneView() {
        sceneView.removeFromSuperview()
        sceneView = nil
        self.view.backgroundColor = UIColor.white
        //self.view.translatesAutoresizingMaskIntoConstraints = false
    }

    private func addScoreLabel(for score: Int) {
        scoreLabel = UILabel()
        scoreLabel.translatesAutoresizingMaskIntoConstraints = false
        scoreLabel.text = "Your score: \(score)"
        scoreLabel.textAlignment = .center
        scoreLabel.font = UIFont.boldSystemFont(ofSize: 30.0)
        view.addSubview(scoreLabel)
    }

    private func setAutoLayout() {
        NSLayoutConstraint.activate([
            scoreLabel.topAnchor.constraint(equalTo: view.topAnchor),
            scoreLabel.leftAnchor.constraint(equalTo: view.leftAnchor),
            scoreLabel.rightAnchor.constraint(equalTo: view.rightAnchor),
            scoreLabel.heightAnchor.constraint(equalToConstant: 150),

            resultsTableView.topAnchor.constraint(equalTo: scoreLabel.bottomAnchor),
            resultsTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            resultsTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            resultsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
    }

    private func addResultsTableView() {
        resultsTableView = UITableView()
        resultsTableView.translatesAutoresizingMaskIntoConstraints = false
        resultsTableView.dataSource = self
        resultsTableView.delegate = self
        resultsTableView.rowHeight = 150.0
        resultsTableView.register(SafetyGameTableCell.self, forCellReuseIdentifier: cellId)
        view.addSubview(resultsTableView)
    }

    override public var shouldAutorotate: Bool {
        return true
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override public var prefersStatusBarHidden: Bool {
        return true
    }
}

extension SafetyGameViewController: UITableViewDataSource, UITableViewDelegate {

    private class SafetyGameTableCell: UITableViewCell {
        var safetyImageView: UIImageView!
        var originalTypeLabel: UILabel!
        var chosenTypeLabel: UILabel!

        override func prepareForReuse() {
            super.prepareForReuse()
            safetyImageView.image = nil
            originalTypeLabel.text = nil
            chosenTypeLabel.text = nil
        }

        func setImageView(with imageName: String) {
            let image = UIImage(named: imageName)
            safetyImageView = UIImageView()
            safetyImageView.image = image
            safetyImageView.contentMode = .scaleAspectFit
            safetyImageView.translatesAutoresizingMaskIntoConstraints = false
            self.contentView.addSubview(safetyImageView)
        }

        private func getChoiceString(for type: Int) -> String {
            if type == 1 {
                return "'Forbidden'"
            } else if type == 2 {
                return "'Cabin luggage'"
            } else if type == 3 {
                return "'Checked-in luggage'"
            } else {
                return "'Prepare for screening'"
            }
        }

        func setLabels(originalType: Int, chosenType: Int) {
            let chosenText = "You chose " + self.getChoiceString(for: chosenType)
            var originalText = ""
            if originalType == chosenType {
                originalText =  "✅ Good job!"
            } else {
                originalText = "🚫 You should have chosen " + getChoiceString(for: originalType)
            }
            chosenTypeLabel = UILabel()
            chosenTypeLabel.numberOfLines = 3
            chosenTypeLabel.text = chosenText
            chosenTypeLabel.textAlignment = .center
            chosenTypeLabel.font = chosenTypeLabel.font.withSize(20.0)
            chosenTypeLabel.lineBreakMode = .byWordWrapping
            chosenTypeLabel.minimumScaleFactor = 2.0
            chosenTypeLabel.adjustsFontSizeToFitWidth = true
            chosenTypeLabel.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(chosenTypeLabel)

            originalTypeLabel = UILabel()
            originalTypeLabel.numberOfLines = 3
            originalTypeLabel.text = originalText
            originalTypeLabel.textAlignment = .center
            originalTypeLabel.font = chosenTypeLabel.font.withSize(20.0)
            originalTypeLabel.lineBreakMode = .byWordWrapping
            originalTypeLabel.minimumScaleFactor = 2.0
            originalTypeLabel.adjustsFontSizeToFitWidth = true
            originalTypeLabel.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(originalTypeLabel)
        }

        // TODO: middle label width
        func setAutoLayout() {
            NSLayoutConstraint.activate([
                safetyImageView.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                safetyImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
                safetyImageView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -10),
                safetyImageView.widthAnchor.constraint(equalToConstant: 130),

                chosenTypeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                chosenTypeLabel.leftAnchor.constraint(equalTo: safetyImageView.rightAnchor, constant: 30),
                chosenTypeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
                chosenTypeLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 130),

                originalTypeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
                originalTypeLabel.leftAnchor.constraint(equalTo: chosenTypeLabel.rightAnchor, constant: 30),
                originalTypeLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20),
                originalTypeLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 10),
                originalTypeLabel.widthAnchor.constraint(greaterThanOrEqualToConstant: 130),

                ])
        }


        override func awakeFromNib() {
            super.awakeFromNib()

            // Initialization code
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.resultsFromScene.count
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SafetyGameTableCell
        cell.setImageView(with: resultsFromScene[indexPath.row].image)
        cell.setLabels(originalType: resultsFromScene[indexPath.row].type, chosenType: resultsFromScene[indexPath.row].typeChosen)
        cell.setAutoLayout()
        return cell
    }
}
