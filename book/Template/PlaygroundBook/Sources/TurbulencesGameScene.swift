//
//  TurbulenceGameScene.swift
//  TestingForPlaygroundBook
//
//  Created by Adam Niepokój on 18/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import SpriteKit

public enum TurbulenceStrength: String {
    case light, moderate, severe
}

class TurbulenceNode: SKSpriteNode {
    var scoreNode: SKSpriteNode!
}


class TurbulenceGameScene: SKScene, SKPhysicsContactDelegate {

    private var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }
    private var hasAvoidedTurbulence = true
    public var turbulenceStrengthString = "moderate"
    public var turbulenceStrength = TurbulenceStrength.moderate
    public var isRunning = false

    private var planeNode: SKSpriteNode!
    private var downButton: UIButton!
    private var upButton: UIButton!
    private var scoreLabel: SKLabelNode!
    private var clouds = [SKSpriteNode]()
    private var endGameFrame: SKSpriteNode!
    private var turbulences = [TurbulenceNode]()
    private var scoreDetectors = [SKSpriteNode]()
    private var scalingFactor: CGFloat!
    private let cloudsTexture = SKTexture(imageNamed: "longbackground")

    private func reloadGame() {
        scene?.removeAllChildren()
        scene?.removeAllActions()
        createPlane()
        createClouds()
        if speed == 0 {
            createButtons()
        }
    }

    public func startGame() {
        if isRunning {
            reloadGame()
        }
        createScoreLabel()
        score = 0
        speed = 1.0
        createTurbulences()
        isRunning = true
    }

    private func checkScore() {
        if score > 9 {
            endGame(with: "You won! 🏅")
        } else if score < -9 {
            endGame(with: "You lost! 😢")
        }
    }

    //TODO: moving speed for turbulence and background
    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        scalingFactor = self.size.height / oldSize.height

        for cloud in clouds {
            cloud.scale(to: CGSize(width: cloud.size.width*scalingFactor, height: self.size.height))
            cloud.position = cloud.position.scaleBy(scalingFactor)
        }

        guard let plane = self.planeNode else { return }
        plane.scale(to: CGSize(width: plane.size.width*scalingFactor, height: plane.size.height*scalingFactor))
        plane.position = plane.position.scaleBy(scalingFactor)

        // TODO: polish
        for turbulence in turbulences {
            turbulence.position = turbulence.position.scaleBy(scalingFactor)
            turbulence.scale(to: CGSize(width: turbulenceRadius*2, height: turbulenceRadius*2))
        }

        for scoredetector in scoreDetectors {
            scoredetector.position = scoredetector.position.scaleBy(scalingFactor)
            scoredetector.scale(to: CGSize(width: scoredetector.size.width, height: size.height))
        }

        guard let scoreLabel = self.scoreLabel else {
            guard let endGameFrame = self.endGameFrame else {
                return
            }
            endGameFrame.position = CGPoint(x: size.width/2, y: size.height/2)
            endGameFrame.scale(to: CGSize(width: endGameFrame.size.width*scalingFactor, height: endGameFrame.size.height*scalingFactor))
            return
        }
        scoreLabel.fontSize = fontSize
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height - 100)
    }

    override func didMove(to view: SKView) {
        createPlane()
        createClouds()
        createButtons()

        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        physicsWorld.contactDelegate = self
    }

}

// MARK: moving plane
extension TurbulenceGameScene {
    private func hitTurbulence() {
        if planeNode.position.y < (0.1)*self.size.height {
            print("too low")
        } else if planeNode.position.y < (0.2)*self.size.height {
            let moveDownAnimation = SKAction.moveBy(x: 0.0, y: -altitudeChange, duration: 0.5)
            planeNode.run(moveDownAnimation)
        } else {
            let moveDownAnimation = SKAction.moveBy(x: 0.0, y: -2*altitudeChange, duration: 0.5)
            planeNode.run(moveDownAnimation)
        }
    }

    @objc private func movePlaneUp() {
        if planeNode.position.y > (0.8)*self.size.height {
            print("too high")
        } else {
            let moveUpAnimation = SKAction.moveBy(x: 0.0, y: altitudeChange, duration: 0.5)
            planeNode.run(moveUpAnimation)
        }
    }

    @objc private func movePlaneDown() {
        if planeNode.position.y < (0.1)*self.size.height {
            print("too low")
        } else {
            let moveDownAnimation = SKAction.moveBy(x: 0.0, y: -altitudeChange, duration: 0.5)
            planeNode.run(moveDownAnimation)
        }
    }

    private func shake(node: SKNode, numberOfShakes: Float) {
        var shakes = [SKAction]()
        for _ in 1...Int(numberOfShakes) {
            let moveX = Float.random(in: 0...20)/2
            let moveY = Float.random(in: 0...20)/2
            let shake = SKAction.moveBy(x: CGFloat(moveX), y: CGFloat(moveY), duration: 0.01)
            shake.timingMode = .easeOut
            shakes.append(shake)
            shakes.append(shake.reversed())
        }
        let sequence = SKAction.sequence(shakes)
        node.run(sequence)
    }
}

// MARK: sprites handling
extension TurbulenceGameScene {
    private func createClouds() {
        scalingFactor = self.size.height / cloudsTexture.size().height
        for parts in 0..<2 {
            let cloud = SKSpriteNode(texture: cloudsTexture)
            cloud.scale(to: CGSize(width: cloud.size.width, height: self.size.height))
            cloud.zPosition = -20
            cloud.anchorPoint = .zero

            cloud.position = CGPoint(
                x: (cloud.frame.width * CGFloat(parts)) - CGFloat(1 * parts),
                y: 0)

            addChild(cloud)
            clouds.append(cloud)
            let scrollLeft = SKAction.moveBy(x: -cloud.frame.width, y: 0, duration: 25)
            let reset = SKAction.moveBy(x: cloud.frame.width, y: 0, duration: 0)
            let sequence = SKAction.sequence([scrollLeft, reset])

            let cloudsLoop = SKAction.repeatForever(sequence)
            cloud.run(cloudsLoop)
        }
    }

    private func createTurbulences() {
        var frequency: TimeInterval = 3

        switch turbulenceStrength {
        case .light:
            frequency = 3
        case .moderate:
            frequency = 2
        case .severe:
            frequency = 1.5
        }
//        if strength == "light" {
//            turbulenceStrength = .light
//            frequency = 3
//        } else if strength == "medium" {
//            turbulenceStrength = .medium
//            frequency = 2
//        } else if strength == "heavy" {
//            turbulenceStrength = .heavy
//            frequency = 1.5
//        }

        let createTurbulence = SKAction.run {
            self.createSingleTurbulence(with: self.turbulenceStrength)
        }
        let pause = SKAction.wait(forDuration: frequency)
        let sequence = SKAction.sequence([createTurbulence, pause])
        let shakeIt = SKAction.repeatForever(sequence)
        run(shakeIt)
    }

    private func createSingleTurbulence(with strength: TurbulenceStrength) {
        let turbulenceNode = TurbulenceNode(imageNamed: strength.rawValue)
        let scoreDetector = SKSpriteNode(color: #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 0), size: CGSize(width: 5, height: size.height))

        turbulenceNode.size = CGSize(width: turbulenceRadius*2, height: turbulenceRadius*2)
        turbulenceNode.name = "turbulence"
        turbulenceNode.physicsBody = SKPhysicsBody(circleOfRadius: turbulenceRadius)
        turbulenceNode.physicsBody?.isDynamic = false

        scoreDetector.name = "scoreDetector"
        scoreDetector.physicsBody = SKPhysicsBody(rectangleOf: scoreDetector.size, center: CGPoint(x: scoreDetector.size.width/2, y: scoreDetector.size.height/2))
        scoreDetector.physicsBody?.isDynamic = false

        turbulenceNode.scoreNode = scoreDetector
        addChild(scoreDetector)
        addChild(turbulenceNode)

        let xCoordinate = size.width + turbulenceRadius
        let yCooridante = CGFloat.random(in: turbulenceRadius...size.height-turbulenceRadius)

        turbulenceNode.position = CGPoint(x: xCoordinate, y: yCooridante)
        scoreDetector.position = CGPoint(x: xCoordinate + turbulenceRadius, y: 0)
        turbulences.append(turbulenceNode)
        scoreDetectors.append(scoreDetector)


        let endPosition = size.width + (turbulenceRadius * 4)
        //TODO: speed
        let moveAction = SKAction.moveBy(x: -endPosition, y: 0, duration: 7)
        let moveSequence = SKAction.sequence([
            moveAction,
            SKAction.removeFromParent()
            ])
        turbulenceNode.run(moveSequence)
        scoreDetector.run(moveSequence)
    }

    private func createPlane() {

        let planeFrame1 = SKTexture(imageNamed: "planeRed1")
        let planeFrame2 = SKTexture(imageNamed: "planeRed2")
        let planeFrame3 = SKTexture(imageNamed: "planeRed3")

        let animationSequence = SKAction.animate(with: [planeFrame1, planeFrame2, planeFrame3, planeFrame2], timePerFrame: 0.02)
        let loopedAnimation = SKAction.repeatForever(animationSequence)

        planeNode = SKSpriteNode(texture: planeFrame1)
        planeNode.zPosition = 10
        planeNode.name = "plane"
        planeNode.run(loopedAnimation)
        let scale = planeFrame1.size().width/planeFrame1.size().height
        planeNode.scale(to: CGSize(width: scale*self.size.height/10, height: self.size.height/10))
        planeNode.position = CGPoint(x: self.size.width / 10, y: self.size.height/2)


        addChild(planeNode)
        planeNode.physicsBody = SKPhysicsBody(circleOfRadius: planeNode.size.width/2)
        planeNode.physicsBody!.contactTestBitMask = planeNode.physicsBody!.collisionBitMask
        planeNode.physicsBody?.isDynamic = true
        planeNode.physicsBody?.collisionBitMask = 0
    }

    private func createScoreLabel() {
        scoreLabel = SKLabelNode(fontNamed: UIFont.boldSystemFont(ofSize: fontSize).fontName)
        scoreLabel.text = "Score: 0"
        scoreLabel.fontColor = UIColor.black
        scoreLabel.fontSize = fontSize
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height - 100)

        addChild(scoreLabel)
    }

    private func endGame(with state: String) {
        scoreLabel = nil
        scene?.backgroundColor = UIColor.white
        endGameFrame = SKSpriteNode()
        let label = SKLabelNode(fontNamed: UIFont.boldSystemFont(ofSize: fontSize).fontName)
        label.fontSize = fontSize

        endGameFrame.position = CGPoint(x: frame.midX, y: frame.midY)
        label.fontColor = UIColor.black

        speed = 0
        label.text = state
        endGameFrame.addChild(label)
        for child in scene!.children {
            child.removeFromParent()
        }
        upButton.removeFromSuperview()
        downButton.removeFromSuperview()
        addChild(endGameFrame)
    }

    private func createButtons() {
        guard let skView = self.view else { return }
        upButton = UIButton()
        downButton = UIButton()

        upButton.setImage(UIImage(named: "upButton"), for: .normal)
        upButton.addTarget(self, action: #selector(TurbulenceGameScene.movePlaneUp), for: .touchDown)
        upButton.translatesAutoresizingMaskIntoConstraints = false
        skView.addSubview(upButton)

        downButton.setImage(UIImage(named: "downButton"), for: .normal)
        downButton.addTarget(self, action: #selector(TurbulenceGameScene.movePlaneDown), for: .touchDown)
        downButton.translatesAutoresizingMaskIntoConstraints = false
        skView.addSubview(downButton)

        NSLayoutConstraint.activate([
            upButton.bottomAnchor.constraint(equalTo: skView.bottomAnchor, constant: -75),
            upButton.rightAnchor.constraint(equalTo: skView.centerXAnchor, constant: -10),
            upButton.widthAnchor.constraint(equalToConstant: 200),
            upButton.heightAnchor.constraint(equalToConstant: 75),

            downButton.bottomAnchor.constraint(equalTo: skView.bottomAnchor, constant: -75),
            downButton.leftAnchor.constraint(equalTo: skView.centerXAnchor, constant: 10),
            downButton.widthAnchor.constraint(equalToConstant: 200),
            downButton.heightAnchor.constraint(equalToConstant: 75)

            ])

    }
}

// MARK: Computed properties
extension TurbulenceGameScene {
    private var turbulenceRadius: CGFloat {
        get {
            switch turbulenceStrength {
            case .light:
                return size.height/10
            case .moderate:
                return size.height/7
            case .severe:
                return size.height/5
            }
        }
    }

    public var altitudeChange: CGFloat {
        get {
            return self.size.height/8
        }
    }

    private var shorterSideLength: CGFloat {
        get {
            if self.size.width < self.size.height {
                return self.size.width
            } else {
                return self.size.height
            }
        }
    }

    private var fontSize: CGFloat {
        get {
            return shorterSideLength/30
        }
    }
}

extension TurbulenceGameScene {

    func didBegin(_ contact: SKPhysicsContact) {
        if contact.bodyA.node?.name == "scoreDetector" || contact.bodyB.node?.name == "scoreDetector", hasAvoidedTurbulence {
            if contact.bodyA.node == planeNode {
                contact.bodyB.node?.removeFromParent()
            } else {
                contact.bodyA.node?.removeFromParent()
            }
            score += 1
            checkScore()
            return
        }

        guard contact.bodyA.node != nil && contact.bodyB.node != nil else {
            return
        }

        if contact.bodyA.node?.name == "turbulence" || contact.bodyB.node?.name == "turbulence" {
            hasAvoidedTurbulence = false
            //self.shake(node: planeNode, numberOfShakes: 10)
            if let turbulence = contact.bodyA.node as? TurbulenceNode {
                turbulence.scoreNode.removeFromParent()
            } else if let turbulence = contact.bodyB.node as? TurbulenceNode {
                turbulence.scoreNode.removeFromParent()
            }
            self.hitTurbulence()
            score -= 1
            checkScore()
            return
        }
    }

    func didEnd(_ contact: SKPhysicsContact) {
        hasAvoidedTurbulence = true
    }
}

extension CGPoint {
    func scaleBy(_ scale: CGFloat) -> CGPoint {
        return CGPoint(x: self.x*scale, y: self.y*scale)
    }
}
