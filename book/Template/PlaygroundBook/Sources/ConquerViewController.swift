//
//  GameViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 19/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import Foundation
import PlaygroundSupport

public class ConquerViewController: LiveViewController {
    
    @IBOutlet weak var label: UILabel!
    public override func viewDidLoad() {
        super.viewDidLoad()
        showChecklist()
        label.text = "🔥"
    }
    
    override public func receive(_ message: PlaygroundValue) {
        //        Uncomment the following to be able to receive messages from the Contents.swift playground page. You will need to define the type of your incoming object and then perform any actions with it.
        //
        //        guard case .data(let messageData) = message else { return }
        //        do { if let incomingObject = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(messageData) as? /*TypeOfYourObject*/ {
        //
        //                //do something with the incoming object from the playground page here
        //
        //            }
        //        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }
        
    }

    private func showChecklist() {
        // TODO: create a checklist for safety precautions
    }
}
