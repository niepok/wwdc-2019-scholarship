//
//  SimulatorScene.swift
//  Book_Sources
//
//  Created by Adam Niepokój on 22/03/2019.
//

import Foundation
import SpriteKit
import CoreMotion

class Plane {
    let finalPosition  = (x: 0.0, y: 0.0, z: 0.0)
    let slopeTopPosition = (
        x: 20.0 * 1852, // 20 nautical miles -> meters
        y: 0.0, //
        z: 6000.0 * 0.3048 // height of point of contact, 6000 ft * 0.3048 -> meters
    )
    // TODO: think about speed
    var speed: Double = 150.0 * 0.514444 // 150 knots to m/s -> Boeing 737 on touchdown

    var a: Double {
        return slopeTopPosition.z/slopeTopPosition.x
    }

    var startTime: Date
    let startPosition: (x: Double, y: Double, z: Double)

    var currentPosition: (x: Double, y: Double, z: Double)

    var currentTargetPosition: (x: Double, y: Double, z: Double) {
        return (x: currentPosition.x,
                y: 0.0,
                z: currentPosition.x * a)
    }

    // horizontal distance to airport
    var distanceToAirport: Double {
        return currentPosition.x
    }

    // current height
    var height: Double {
        return currentPosition.z
    }

    // current horizontal deviation
    var inclinationY: Double {
        return currentPosition.y
    }

    var currentYtangens: Double {
        return currentPosition.y / currentPosition.x
    }

    var currentYAngle: Double {
        return atan(currentYtangens)
    }

    //current vertical deviation
    var inclinationZ: Double {
        return currentPosition.z - currentTargetPosition.z
    }

    init(startPosition: (x: Double, y: Double, z: Double)) {
        self.startPosition = startPosition
        self.startTime = Date()
        self.currentPosition = startPosition
        //print(currentPosition)
    }

    public var startingAngleY: Double?
    public var startingAngleZ: Double?

    func updatePosition(_ yAngle: Double, _ zAngle: Double) {
        let vx = speed * cos(yAngle-startingAngleY!)
        let vy = speed * sin(yAngle-startingAngleY!)
        let vz = speed * sin(zAngle-startingAngleZ!)

        currentPosition.x = currentPosition.x - vx*frequency
        currentPosition.y = currentPosition.y - vy*frequency
        currentPosition.z = currentPosition.z - vz*frequency
        if hasReachedTheGround || hasReachedTheAirport {
            hasEndedFlight = true
        }
        //print("y: \(inclinationY), z: \(inclinationZ)")
    }

    private var hasReachedTheGround: Bool {
        return currentPosition.z <= 0
    }

    private var hasReachedTheAirport: Bool {
        return currentPosition.x <= 0
    }

    public var hasEndedFlight = false

    private let papiLightHeight = 50.0
    private let deviationFactor = tan(0.8)

    public var testForPapi: (Bool, Bool, Bool, Bool) {
        if inclinationZ > papiLightHeight {
            return (true, true, true, true)
        } else if inclinationZ < -papiLightHeight {
            return (false, false, false, false)
        } else {
            return (true, true, false, false)
        }
    }

    public var flightStatus: String {
        var status = ""
        if hasEndedFlight {
            status = "Your flight has ended! ✈️\n"
            if hasReachedTheGround, hasReachedTheAirport {
                status += "Congrats! You made it safe to the airport! 🛬"
            } else if hasReachedTheAirport {
                if currentPosition.z <= 30 {
                    status += "Congrats! You made it safe to the airport! 🛬"
                } else {
                    status += "You flew over the airport 😢"
                }
            } else if hasReachedTheGround {
                status += "You crashed before the airport! 💥😢"
            }
        } else {
            status = "Your coordinates: \(currentPosition)"
        }
        return status
    }
}

public let frequency = 1.0 / 40.0

class SimulatorScene: SKScene {

    private class PapiNode: SKSpriteNode {
        let redTexture = SKTexture(imageNamed: "sim-papi-red")
        let whiteTexture = SKTexture(imageNamed: "sim-papi-white")

        var state = true {
            didSet {
                self.texture = state ? whiteTexture : redTexture
            }
        }
    }

    private let inclinationSpinnerNode = SKSpriteNode(imageNamed: "sim-inclination-spinner")
    private let inclinationSpinnerNode2 = SKSpriteNode(imageNamed: "sim-inclination-spinner-two")
    private let inclinationMiddleNode = SKSpriteNode(imageNamed: "sim-middle")
    private let inclinationBounds = SKSpriteNode(imageNamed: "sim-inclination-bounds")
    private let inclinationSteadyNode = SKSpriteNode(imageNamed: "sim-inclination-steady")
    private let inclinationSteadyNode2 = SKSpriteNode(imageNamed: "sim-inclination-steady-two")
    private let slopeNode = SKSpriteNode(imageNamed: "sim-slope")
    private let slopeIndicatorNode = SKSpriteNode(imageNamed: "sim-slope-indicator")
    private var slopeBounds: SKSpriteNode!
    private let courseNode = SKSpriteNode(imageNamed: "sim-course")
    private let courseIndicatorNode = SKSpriteNode(imageNamed: "sim-course-indicator")
    private var courseBounds: SKSpriteNode!
    private let courseDistanceNode = SKSpriteNode(imageNamed: "sim-course-distance")
    private let courseDistanceIndicator = SKSpriteNode(imageNamed: "sim-course-distance-indicator")
    private var labelNode: SKLabelNode!
    private let papiFrame = SKSpriteNode(imageNamed: "sim-papi-frame")
    private var papiNodes = [
        PapiNode(imageNamed: "sim-papi-white"),
        PapiNode(imageNamed: "sim-papi-white"),
        PapiNode(imageNamed: "sim-papi-white"),
        PapiNode(imageNamed: "sim-papi-white")
    ]

    private let heightMeterNode = SKSpriteNode(imageNamed: "sim-meter-frame")
    private let distanceMeterNode = SKSpriteNode(imageNamed: "sim-meter-frame")

    private var endGameNode: SKSpriteNode!

    public var startButton: UIButton!

    private var motionManager = CMMotionManager()
    private let middle = CGPoint(x: 0.5, y: 0.5)
    private var plane: Plane!
    public var startPosition = (x: 1000.0, y: 0.0, z: 500.0)// (x: 24000.0, y: 400.0, z: 1500.0)
    private var gameStarted = false
    private var indicatorRange = 600.0

    private var shorterSideLength: CGFloat {
        get {
            if self.size.width < self.size.height {
                return self.size.width
            } else {
                return self.size.height
            }
        }
    }

    private var lastRoll: Double?
    private var lastPitch: Double?
    private var lastYaw: Double?
    private var lastYdelta = 0.0

    public var previousOrientation = UIDeviceOrientation.unknown
    public var playgroudOrientation = PlaygroundOrientation.unknown

    public var isStartEnabled = false

    override func didMove(to view: SKView) {
        scene?.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        addNodesToScene()
        startButton.isEnabled = isStartEnabled
        physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        physicsWorld.contactDelegate = self
        startQueuedUpdates()
    }

    override func willMove(from view: SKView) {
        motionManager.stopDeviceMotionUpdates()
    }

    override func didChangeSize(_ oldSize: CGSize) {
        super.didChangeSize(oldSize)
        inclinationSteadyNode.position = CGPoint(x: size.width/2, y: size.height/2)
        inclinationSteadyNode.scale(to: CGSize(
            width: size.height/2*inclinationSteadyNode.size.width/inclinationSteadyNode.size.height,
            height: size.height/2))

        guard self.endGameNode != nil else {return}
        endGameNode.position = CGPoint(x: size.width/2, y: size.height/2)
        endGameNode.scale(to: CGSize(width: shorterSideLength/2, height: shorterSideLength/3))

    }

    private func addNodesToScene() {
        addInclination()
        addSlope()
        addCourse()
        addPapis()
        //addLabel()
        addStartButton()
        addMeters()
    }

    private func addStartButton() {
        if startButton == nil {
            startButton = UIButton()

            startButton.setTitle("Lock screen\nrotation,\nclick here\nand FLY!", for: .normal)
            startButton.titleLabel?.numberOfLines = 4
            startButton.addTarget(self, action: #selector(SimulatorScene.startGame), for: .touchDown)
            startButton.backgroundColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 0.6140839041)
            startButton.layer.cornerRadius = 30.0
            startButton.setTitleColor(UIColor.black, for: .normal)
            startButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 30)
            startButton.titleLabel?.textAlignment = .center
            startButton.translatesAutoresizingMaskIntoConstraints = false
            guard let skView = self.view else { return }
            skView.addSubview(startButton)

            NSLayoutConstraint.activate([
                startButton.centerXAnchor.constraint(equalTo: skView.centerXAnchor),
                startButton.centerYAnchor.constraint(equalTo: skView.centerYAnchor),
                startButton.widthAnchor.constraint(equalToConstant: 200),
                startButton.heightAnchor.constraint(equalToConstant: 200),
                ])

        }

    }

    @objc private func startGame() {
        if playgroudOrientation == .unknown {
            startButton.titleLabel?.text = "Adjust position,\n\nclick here\nand FLY!"
            startButton.titleLabel?.numberOfLines = 3
        } else {
            plane = Plane(startPosition: self.startPosition)
            gameStarted = true
            startButton.isEnabled = false
            startButton.removeFromSuperview()
            startButton = nil
        }
    }

    private var finishedUpdating = false

    override func update(_ currentTime: TimeInterval) {
        guard self.plane != nil
            else { return }
        if !finishedUpdating {
            if plane.hasEndedFlight {
                addEndGameFrame(with: plane.flightStatus)
            } else {
                updatePapi()
                updateSlope()
                updateDeviation()
                updateMeters()
            }
        }
    }

    private func updatePapi() {
        let papis = plane.testForPapi
        papiNodes[0].state = papis.0
        papiNodes[1].state = papis.1
        papiNodes[2].state = papis.2
        papiNodes[3].state = papis.3
    }

    private func updateSlope() {
        // zmienić znak
        if plane.inclinationZ <= -indicatorRange {
            slopeIndicatorNode.position.y = CGFloat(indicatorRange)
        } else if plane.inclinationZ >= indicatorRange{
            slopeIndicatorNode.position.y = CGFloat(-indicatorRange)
        } else {
            slopeIndicatorNode.position.y = -CGFloat(plane.inclinationZ)
        }

    }

    private func updateDeviation() {
        // bez zmiany znaku
        if plane.inclinationY <= -indicatorRange {
            courseIndicatorNode.position.x = CGFloat(indicatorRange)
        } else if plane.inclinationY >= indicatorRange{
            courseIndicatorNode.position.x = CGFloat(-indicatorRange)
        } else {
            courseIndicatorNode.position.x = CGFloat(plane.inclinationY)
        }
    }

    private func updateMeters() {
        (heightMeterNode.childNode(withName: "value") as? SKLabelNode)?.text = String(format: "%.2f", plane.currentPosition.z)
        (distanceMeterNode.childNode(withName: "value") as? SKLabelNode)?.text = String(format: "%.2f", plane.currentPosition.x)
    }
}

// MARK: motion relate
extension SimulatorScene {
    private var yDeltaConst: CGFloat {
        return size.height/10
    }

    //    TODO: Important
    //    If your app relies on the presence of accelerometer hardware, configure
    //    the UIRequiredDeviceCapabilities key of its Info.plist file with the accelerometer value. For more information about the meaning of this key, see Information Property List Key Reference.
    private func startQueuedUpdates() {
        if motionManager.isDeviceMotionAvailable {
            self.motionManager.deviceMotionUpdateInterval = frequency
            self.motionManager.showsDeviceMovementDisplay = true
            self.motionManager.startDeviceMotionUpdates(
                using: .xArbitraryZVertical,
                to: .main, withHandler: { (data, error) in
                    if let validData = data {
                        let roll = validData.attitude.roll
                        let pitch = validData.attitude.pitch
                        let yaw = validData.attitude.yaw

                        if !self.gameStarted {
                            self.playgroudOrientation = self.checkOrientation(validData.gravity)
                        }

                        if self.lastRoll == nil {
                            self.lastRoll = roll
                        }
                        if self.lastPitch == nil {
                            self.lastPitch = pitch
                        }
                        if self.lastYaw == nil {
                            self.lastYaw = yaw
                        }

                        self.handleMotionData(roll, pitch, yaw)
                    }
            })
        }
    }


    private func handleMotionData(_ roll: Double, _ pitch: Double, _ yaw: Double) {
        var yDelta = CGFloat(0.0)
        var zDelta = CGFloat(0.0)
        var yDeltaR = CGFloat(0.0)
        switch playgroudOrientation {
        case .portrait:
            yDelta = yDeltaConst * CGFloat((tan(pitch)-tan(lastPitch!)))
            yDeltaR = CGFloat(pitch)
            zDelta = CGFloat(roll)
        case .portraitUpsideDown:
            yDelta = yDeltaConst * CGFloat((tan(pitch)-tan(lastPitch!)))
            yDeltaR = CGFloat(-pitch)
            zDelta = CGFloat(-roll)
        case .landscapeLeft:
            yDelta = yDeltaConst * CGFloat((tan(roll)-tan(lastRoll!)))
            yDeltaR = CGFloat(roll)
            zDelta = CGFloat(pitch)
        case .landscapeRight:
            yDelta = yDeltaConst * CGFloat((tan(-roll)-tan(-lastRoll!)))
            yDeltaR = CGFloat(-roll)
            zDelta = CGFloat(-pitch)
        // TODO: adjust according to previuos
        case .faceUp:
            zDelta = CGFloat(-yaw)
            yDelta = yDeltaConst * CGFloat((tan(roll)-tan(lastRoll!)))
            yDeltaR = CGFloat(roll)
        case .faceDown:
            zDelta = CGFloat(yaw)
            yDelta = yDeltaConst * CGFloat((tan(roll)-tan(lastRoll!)))
            yDeltaR = CGFloat(roll)
        case .unknown:
            return
        }

        // TODO: make it slower
        if abs(self.inclinationMiddleNode.position.y + yDelta) < 6 * yDeltaConst && gameStarted {
            self.inclinationMiddleNode.position.y = self.inclinationMiddleNode.position.y + yDelta
        } else {
            yDeltaR = CGFloat(self.lastYdelta)
        }

        self.inclinationMiddleNode.zRotation = zDelta

        if plane != nil, !plane.hasEndedFlight {
            if plane.startingAngleY == nil {
                plane.startingAngleY = Double(zDelta)
                plane.startingAngleZ = Double(yDeltaR)
            }
            plane.updatePosition(Double(zDelta), Double(yDeltaR))
        }

        self.lastRoll = roll
        self.lastYaw = yaw
        self.lastPitch = pitch
    }
}

// MARK: Playground orientation
extension SimulatorScene {
    enum PlaygroundOrientation: Int {
        case unknown = 0
        case portrait
        case portraitUpsideDown
        case landscapeLeft
        case landscapeRight
        case faceUp
        case faceDown
    }

    public func checkOrientation(_ gravity: CMAcceleration) -> PlaygroundOrientation {
        // x bliskie 1 = landscape right
        // x bliskie -1 = landscape left
        // y bliskie y = portrait upside
        // y bliskie -1 = landscape
        // z -1 faceUp
        // z 1 faceDown
        let gravityFactor = 0.8
        let axis = [gravity.x, gravity.y, gravity.z]
        let max = axis.max()!
        let min = axis.min()!
        if gravity.z > gravityFactor && gravity.z == max {
            return .faceDown
        } else if gravity.z < -gravityFactor && gravity.z == min {
            return .faceUp
        } else if gravity.x > gravityFactor && gravity.x == max {
            return .landscapeRight
        } else if gravity.x < -gravityFactor && gravity.x == min {
            return .landscapeLeft
        } else if gravity.y > gravityFactor && gravity.y == max {
            return .portraitUpsideDown
        } else if gravity.y < -gravityFactor && gravity.y == min {
            return .portrait
        } else {
            return .unknown
        }
    }

    private func didChagedOrientation(to: PlaygroundOrientation) {

    }

}

// MARK: Sprites handling
extension SimulatorScene {
    private func addInclination() {
        inclinationSteadyNode.anchorPoint = middle
        addChild(inclinationSteadyNode)

        inclinationMiddleNode.anchorPoint = middle
        inclinationMiddleNode.zPosition = 1
        inclinationSteadyNode.addChild(inclinationMiddleNode)

        inclinationSteadyNode2.anchorPoint = middle
        inclinationSteadyNode2.zPosition = 3
        inclinationSteadyNode.addChild(inclinationSteadyNode2)

        inclinationSpinnerNode2.anchorPoint = middle
        inclinationSpinnerNode2.zPosition = 1
        inclinationMiddleNode.addChild(inclinationSpinnerNode2)

        inclinationBounds.anchorPoint = middle
        inclinationBounds.zPosition = -2
        inclinationSteadyNode2.addChild(inclinationBounds)

        //        inclinationMiddleNode.physicsBody = SKPhysicsBody(texture: inclinationMiddleNode.texture!, size: inclinationMiddleNode.size)
        //        setBoundsProperties(for: inclinationMiddleNode.physicsBody)
        //
        //        inclinationBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: inclinationBounds.frame)
        //        setBoundsProperties(for: inclinationBounds.physicsBody)
    }

    private func setBoundsProperties(for physicsBody: SKPhysicsBody?) {
        guard let body = physicsBody else { return }
        body.friction = 0
        body.restitution = 0
        body.linearDamping = 0
        body.angularDamping = 0
        body.allowsRotation = false
    }

    private func addSlope() {
        slopeNode.anchorPoint = middle
        slopeNode.zPosition = 4
        slopeNode.position = CGPoint(
            x: 900,
            y: 0)
        inclinationSteadyNode.addChild(slopeNode)

        slopeIndicatorNode.anchorPoint = middle
        slopeIndicatorNode.zPosition = 2
        slopeNode.addChild(slopeIndicatorNode)

        slopeBounds = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), size: slopeNode.size)
        slopeBounds.anchorPoint = middle
        slopeBounds.zPosition = 2
        slopeNode.addChild(slopeBounds)
        slopeBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: slopeBounds.frame)
        setBoundsProperties(for: slopeBounds.physicsBody)

        slopeIndicatorNode.physicsBody = SKPhysicsBody(texture: slopeIndicatorNode.texture!, size: slopeIndicatorNode.size)
        setBoundsProperties(for: slopeIndicatorNode.physicsBody)
    }

    private func addCourse() {
        courseNode.anchorPoint = middle
        courseNode.zPosition = 4
        courseNode.position = CGPoint(
            x: 0,
            y: -1000)
        inclinationSteadyNode.addChild(courseNode)

        courseIndicatorNode.anchorPoint = middle
        courseIndicatorNode.zPosition = 2
        courseNode.addChild(courseIndicatorNode)

        courseBounds = SKSpriteNode(color: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0), size: courseNode.size)
        courseBounds.anchorPoint = middle
        courseBounds.zPosition = 2
        courseNode.addChild(courseBounds)
        courseBounds.zRotation = CGFloat.pi/2
        courseBounds.physicsBody = SKPhysicsBody(edgeLoopFrom: slopeBounds.frame)
        setBoundsProperties(for: courseBounds.physicsBody)

        courseIndicatorNode.physicsBody = SKPhysicsBody(texture: courseIndicatorNode.texture!, size: courseIndicatorNode.size)
        setBoundsProperties(for: courseIndicatorNode.physicsBody)

    }

    private func addMeters() {
        heightMeterNode.anchorPoint = middle
        heightMeterNode.zPosition = 4
        heightMeterNode.position = CGPoint(
            x: -800,
            y: 1230)
        inclinationSteadyNode.addChild(heightMeterNode)

        self.add(to: heightMeterNode, label: "Height [m]")


        distanceMeterNode.anchorPoint = middle
        distanceMeterNode.zPosition = 4
        distanceMeterNode.position = CGPoint(
            x: -800,
            y: 960)
        inclinationSteadyNode.addChild(distanceMeterNode)

        self.add(to: distanceMeterNode, label: "Distance [m]")
    }

    private func add(to node: SKSpriteNode, label text: String) {
        let titleLabelNode = SKLabelNode(fontNamed: UIFont.systemFont(ofSize: 10.0).fontName)
        titleLabelNode.zPosition = 1
        titleLabelNode.numberOfLines = 1
        titleLabelNode.position = CGPoint(x: 0, y: 70)
        titleLabelNode.fontSize = 50.0
        titleLabelNode.text = text
        node.addChild(titleLabelNode)

        let valueLabelNode = SKLabelNode(fontNamed: UIFont.systemFont(ofSize: 10.0).fontName)
        valueLabelNode.zPosition = 1
        valueLabelNode.numberOfLines = 1
        valueLabelNode.position = CGPoint(x: 0, y: -60)
        valueLabelNode.fontSize = 110.0
        valueLabelNode.name = "value"
        node.addChild(valueLabelNode)
    }

    private func addPapis() {
        papiFrame.anchorPoint = middle
        papiFrame.zPosition = 4
        papiFrame.position = CGPoint(
            x: 250,
            y: 1100)
        inclinationSteadyNode.addChild(papiFrame)

        var i = 0
        for papi in papiNodes {
            papi.anchorPoint = middle
            let part = CGFloat(-3+2*i)
            papi.position = CGPoint(
                x: part*papiFrame.size.width/8,
                y: 0)
            papi.zPosition = 1
            papiFrame.addChild(papi)
            i += 1
        }
    }

    private func addLabel() {
        labelNode = SKLabelNode(fontNamed: UIFont.systemFont(ofSize: 10.0).fontName)
        labelNode.zPosition = 2
        labelNode.numberOfLines = 5
        labelNode.fontSize = 100.0
        labelNode.text = "test"
        inclinationSteadyNode2.addChild(labelNode)

    }

    private func addEndGameFrame(with state: String) {
        endGameNode = SKSpriteNode()
        endGameNode.position = CGPoint(x: frame.midX, y: frame.midY)
        addChild(endGameNode)

        let label = SKLabelNode(fontNamed: UIFont.boldSystemFont(ofSize: 10.0).fontName)
        label.fontSize = 30.0
        label.numberOfLines = 3
        label.fontColor = UIColor.black
        label.text = state
        endGameNode.addChild(label)

        inclinationSteadyNode.removeFromParent()
        scene?.backgroundColor = UIColor.white
        speed = 0
        motionManager.stopDeviceMotionUpdates()
        finishedUpdating = true
    }
}

extension SimulatorScene: SKPhysicsContactDelegate {

}
