//
//  StoryViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 24/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//
import UIKit
import Foundation
import PlaygroundSupport

public class StoryViewController: LiveViewController {
    
    @IBOutlet weak var nameLabel: UILabel!

    private var stories = [
        "According to a Harvard University study, the odds that your airplane will crash are 1 in 1.2 million, and the odds of dying from a crash are 1 in 11 million. (By comparison, the odds of dying from a shark attack are 1 in 3.1 million.) Transportation by air is far more safe than driving, where your chances of dying in a car accident are 1 in 5,000.",
        "According to the Aviation Safety Network, of the 163 aviation accidents in 2016, only 15 percent resulted in a fatality. In addition, a study conducted by the National Transportation Safety Board that looked at data from 1983 to 2000 showed that more than 80 percent of aviation accident victims survived the crash.",
        "A person could, on average, fly once a day for four million years before succumbing to a fatal crash, according to Arnold Barnett, a professor of statistics at the Massachusetts Institute of Technology (MIT).",
        
    ]

    public override func viewDidLoad() {
        super.viewDidLoad()
        self.nameLabel.text = stories[0]
    }
    
    public override func receive(_ message: PlaygroundValue) {
        guard case .data(let arrayData) = message else { return }
        do {
            if let stringArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(arrayData) as? String {
                if i > 1 {
                    i = 0
                } else {
                    i += 1
                }
                welcome()
            } else {
            }
        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }

    }
    private var i = 0
    private func welcome() {
        nameLabel.text = stories[i]
    }
}
