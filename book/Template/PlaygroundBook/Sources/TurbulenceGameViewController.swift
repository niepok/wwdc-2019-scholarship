//
//  TurbulenceGameViewController.swift
//  TestingForPlaygroundBook
//
//  Created by Adam Niepokój on 18/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import Foundation
import PlaygroundSupport
import SpriteKit

//TODO: launch scene from receive
public class TurbulenceGameViewController: LiveViewController {

    private var turbulenceStrength = TurbulenceStrength.moderate

    public override func receive(_ message: PlaygroundValue) {
        guard case .data(let arrayData) = message else { return }
        do {
            if let stringArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(arrayData) as? String {
                switch stringArray {
                case "light":
                    turbulenceStrength = TurbulenceStrength.light
                case "moderate":
                    turbulenceStrength = TurbulenceStrength.moderate
                case "severe":
                    turbulenceStrength = TurbulenceStrength.severe
                default:
                    return
                }
                launchGame()
            } else {
            }
        } catch let error {
            fatalError("\(error) Unable to receive the message from the Playground page") }

    }

    private func launchGame() {
        if let scene = (self.view as! SKView?)?.scene as? TurbulenceGameScene {
            scene.turbulenceStrength = self.turbulenceStrength
            scene.startGame()
        }
    }

    private func loadScene() {
        if let view = self.view as! SKView? {
            let scene = TurbulenceGameScene()
            scene.size = self.view.bounds.size

            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
        }
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        loadScene()
    }

    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let view = self.view as? SKView {
            view.scene?.size = size
        } else {
            view.frame.size = size
        }

    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let view = self.view as? SKView {
            view.scene?.size = self.view.bounds.size
        } else {
            view.frame.size = self.view.bounds.size
        }
    }

    override public var shouldAutorotate: Bool {
        return false
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override public var prefersStatusBarHidden: Bool {
        return true
    }
}
