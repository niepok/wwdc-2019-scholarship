//#-hidden-code
import Foundation
//#-end-hidden-code
/*:
 The rule **”trust pilot”** from previous page also applies in different situation, which is **bad weather conditions**. This people are trained to be able to control the aircraft in every situation, no matter the conditions. Even when they **can’t see anything outside** the window. But they also have some help. In two systems:
 1. [ILS](glossary://ILS) which role is to help pilot get on the right landing [slope](glossary://slope) that operates on radio waves
 2. [PAPI lights](glossary://PAPI%20lights) which role is to help pilot stay on the right angle of approach

 - Experiment:
 In this game you would be a pilot, whose task is to reach an airport only based on instruments. On the right you can see a simulator of that instruments. Below you would find an instruction how to read them. The controls are based on iPad movement so for better experience Lock Rotation of device now. Specify the position you want to start from (this affects how long would it take you to reach airport). Good luck, fly safe!

 */
//#-code-completion(everything, hide)
let startingHeight: Double = /*#-editable-code root node*/<#from 0.0 to 2 500#>/*#-end-editable-code*/
let startingDistanceToAirport: Double = /*#-editable-code root node*/<#from 0.0 to 10 000.0#>/*#-end-editable-code*/
let startingDistanceToSlope: Double = /*#-editable-code root node*/<#from -500.0 to 500.0#>/*#-end-editable-code*/
//#-hidden-code
sendValue(.data(try NSKeyedArchiver.archivedData(withRootObject: [startingDistanceToAirport,startingDistanceToSlope,startingHeight], requiringSecureCoding: true)))
//#-end-hidden-code
/*:
 - Important:
 Lock Rotation of device before playing!
 
**How to read PAPI:**

 Papi lights have 4 lamps which indicates if the aircraft is on right angle of approach.There are 3 [states](glossary://papi%20states) that we need to look for
 * (4 whites) WHITE on WHITE - "Check your height" (or "You're gonna fly all night") (too high)
 * (2 left whites, 2 right reds) RED on WHITE  – "You're all right"
 * (4 reds) RED on RED – "You're dead" (too low)


**How to read ILS:**

 The ILS indicators are the two rectangles: vertical and horizontal. They both represent the difference between the aircraft position and the airport slope.
 * on both of them the yellow line is a position of slope
 * on both of them the purple shape is the aircraft position relatively to [slope](glossary://slope)
 * vertical indicates difference in height, but the axis is inverted (if you are too high the purple shape appears below yellow line)
 * horizontal indicates distance to slope in horiontal axis, but the axis is inverted (if you are on the left side of slope the purple shape appears on the right of yellow line)
*/
