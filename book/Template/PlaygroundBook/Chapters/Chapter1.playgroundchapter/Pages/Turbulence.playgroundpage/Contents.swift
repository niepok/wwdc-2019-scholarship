//#-hidden-code
import Foundation
import PlaygroundSupport
//#-end-hidden-code
/*:
 It can feel like **the scariest** part of flying, but [turbulence](glossary://turbulence) is no cause for alarm. They are caused by: [turbulent atmosphere](glossary://turbulent%20atmosphere), [hot air](glossary://hot%20air) (thermal turbulence), [landscape](glossary://landscape) (mechanical turbulence)or other [airplanes](glossary://airplanes) (wake turbulence). Pilots can predict when they will appear. They will try to [avoid](glossary://avoid%20turbulence) them. But even if your plane hits turbulence, the danger is really low. Aircrafts are now build to withstand very severe turbulence thanks to: **strengthened body** and **turbulence avoidance system**

 - Important:
 **Turbulence avoidance system** is what makes people scared during the flight. It's a special feature that when plane hits turbulence it can quickly change altitude by 100ft. That's the moment when you "jump" on your seat. The scariest thing is actually... a **safety feature**!

 Let's play a game in which you would be a pilot!

 - Experiment:
 Your task would be to **avoid turbulences** appearing as circles on the screen. Control an airplane with displayed up and down buttons. Be carefull! When you hit a turbulence you will loose height (as explained above) and points. By avoiding turbulence you gain points. Game ends when you eihter get 10 or -10 points. Good luck!

 To change difficulty of game change the value below. Possible vales: _.light, .moderate, .severe_.
 */
//#-code-completion(everything, hide)
//#-code-completion(identifier, show, light, moderate, severe)
let turbulenceStrength: TurbulenceStrength = /*#-editable-code root node*/.<#choose strenght#>/*#-end-editable-code*/
//#-hidden-code
sendValue(.data(try NSKeyedArchiver.archivedData(withRootObject: turbulenceStrength.rawValue, requiringSecureCoding: true)))
//#-end-hidden-code
/*:
 - Important:
 Turbulences haven’t cause any plane crash in over 40 years!


 **What to do regarding turbulence:**
 * Book a seat near the wings and windows (less bumpy)
 * **Trust pilot**
 * Stow your luggage
 * Buckle up
 * [Clench your buttocks](glossary://clench%20buttocks)
 * [Write your name on a piece of paper](glossary://write%20your%20name)
 */







