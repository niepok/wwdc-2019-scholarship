//#-hidden-code
import Foundation
//#-end-hidden-code
/*:
**The goal of this playground** was to show you how particular things in plane travelling world work. Be advised that the things shown here were just a models of some behaviour. When travelling by plane always cooperate with a flight crew, follow the neccessary procedures and... **relax**.

 - Important:
  Travelling by plane is really, really safe, so buy a ticket and go see the world. Take care!
 
_Made by Adam Niepokój as WWDC 2019 Scholarship entry, March 2019_
 */


