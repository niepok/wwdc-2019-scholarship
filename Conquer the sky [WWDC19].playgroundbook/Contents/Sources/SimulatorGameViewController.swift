//
//  SimulatorGameViewController.swift
//  Book_Sources
//
//  Created by Adam Niepokój on 22/03/2019.
//

import UIKit
import Foundation
import PlaygroundSupport
import SpriteKit

//TODO: launch scene from receive
public class SimulatorGameViewController: LiveViewController {

    private var userArray = [
        2000.0, 400.0, 1500.0
        ]

    public override func receive(_ message: PlaygroundValue) {
        guard case .data(let arrayData) = message else { return }
        do {
            if let doubleArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(arrayData) as? [Double] {
                userArray = doubleArray
                isStartEnabled = true
                loadScene()
            }
        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }

    }

    private var isStartEnabled = false

    private func loadScene() {
        if let view = self.view as! SKView? {
            if !view.subviews.isEmpty{
                for v in view.subviews {
                    v.removeFromSuperview()
                }
            }
            let scene = SimulatorScene()
            scene.startPosition = (userArray[0],userArray[1],userArray[2])
            scene.size = self.view.bounds.size
            scene.isStartEnabled = isStartEnabled
            scene.scaleMode = .aspectFill
            view.presentScene(scene)
            view.ignoresSiblingOrder = true
            //view.showsPhysics = true
        }
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
//        UIDevice.current.beginGeneratingDeviceOrientationNotifications()
//        NotificationCenter.default.addObserver(self, selector: #selector(LiveViewController_4_1.rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        loadScene()
    }

//    @objc func rotated() {
//        print("rotatedTo: \(self.view.bounds.size)")
//        print(UIDevice.current.orientation.rawValue)
//        if let view = self.view as? SKView,
//            let scene = view.scene as? SimulatorScene {
//            scene.isRotating = true
//            scene.size = self.view.bounds.size
//        }
//    }

    override public func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let view = self.view as? SKView,
            let scene = view.scene as? SimulatorScene {
            scene.size = size
            print("viewWillTransitionToSize: \(size)")
            print(UIDevice.current.orientation.rawValue)
        } else {
            view.frame.size = size
        }

    }

    override public func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let view = self.view as? SKView {
            view.scene?.size = self.view.bounds.size
        } else {
            view.frame.size = self.view.bounds.size
        }
    }

    override public var shouldAutorotate: Bool {
        return false
    }

    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override public var prefersStatusBarHidden: Bool {
        return true
    }
}
