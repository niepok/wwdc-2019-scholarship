//
//  FlyViewController.swift
//  SafetyGame
//
//  Created by Adam Niepokój on 24/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//
import UIKit
import Foundation
import PlaygroundSupport

public class FlyViewController: LiveViewController {
    
    @IBOutlet weak var label: UILabel!

    public override func viewDidLoad() {
        super.viewDidLoad()
        label.numberOfLines = 3
        label.text = "Thank you!😁\nHave a safe flight!\n✈️✈️✈️"
        
    }
    
//    override public func receive(_ message: PlaygroundValue) {
//        guard case .data(let messageData) = message else { return }
//        do { if let incomingObject = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(messageData) as? String {
//            }
//        } catch let error { fatalError("\(error) Unable to receive the message from the Playground page") }
//    }
}
