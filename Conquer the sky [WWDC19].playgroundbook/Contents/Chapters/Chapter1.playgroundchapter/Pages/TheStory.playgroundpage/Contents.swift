/*:
 Being in the air always fascinated people. Thanks to many great minds such as [Leonardo Da Vinci](glossary://Leonardo%20Da%20Vinci), [the Wright brothers](glossary://the%20Wright%20brothers) or [Frank Whittle](glossary://Frank%20Whittle) (and many others) we are now able to travel to almost every place in the world. This changed the world - made it smaller and more accessible. It’s estimated that in 2019 we would almost reach **40 millions (39.8)** single passenger flights worldwide! **That’s almost 76 machines taking off each minute of a year.** Isn’t that amazing? The amount of planes in the air in every given moment differs depending on time of the day and year, but in top season it can get up to 16 000 commercial airplanes flying over our heads!  Each of this planes is on average filled with 80% of it’s capacity.

 This numbers sound impressive, but still **almost 1 out of 3 people are either scared or anxious about flying**. The reasons for that differ, but they might be categorised into four causes groups:

 * Difficult time during a previous flight
 * Reaction to stories heard
 * Imagining worst case scenarios
 * Other problems which increased discomfort of flying
 * Stress prior to becoming uncomfortable

Stories about plane accidents hit news lines from time to time or bad experiences like severe turbulences might just happen, it’s normal. **Still - travelling on an airplane is extremely safe**. Here are some statistics to prove that:


- Experiment:
Press "Run My Code" to see a statistic information about airplanes safety

 To solve this issues **knowledge** comes with help. Just the fact that the probability of being a victim of an airplane accident is extremely low should tell us something. But if that’s not enough it might be helpful to know why and particular things are happening and how to prepare for them. **This is the goal of this playground** - to show you some things that may allow you to stay calm on your next flight.

 - Important:
 The reason I’m telling you this story is because **it happened also to me**. During one of my flights I experienced very hard turbulence that latermade me scared every time I boarded a plane. **With understanding how things are working I overcome some of my fears and now I’m able to stay calm while in the air**. So if you are ready to do so then fasten your seatbelts - we are taking off!
 */
//#-hidden-code
import Foundation
import PlaygroundSupport
public enum Statictics {
    case accidentsAmount
}
let story = ""
sendValue(.data(try NSKeyedArchiver.archivedData(withRootObject: story, requiringSecureCoding: true)))
//#-end-hidden-code
