//#-hidden-code
import Foundation
import PlaygroundSupport
//#-end-hidden-code
//#-code-completion(everything, hide)
/*:
Safety on board of a plane is something that has been **extremely** improved over the past years. Because of some accidents some procedures have been introduced: [screening with metal detectors](glossary://screening%20with%20metal%20detectors), [x-ray luggage scanning](glossary://x-ray%20luggage%20scanning) or [limiting liquids volume](glossary://limiting%20liquids%20volume). All this procedures are there to protect you while in the airport and in air. They are equal to everyone and are [not to make your harder](glossary://random%20additional%20safety%20check)!

 - Important:
 People are very concerned about they safety and sometimes become to stressed to go through airport with calm. Good preparation can help you enjoy your travel and **fight the anxiety**.

Apart from procedures there are also list of things you may or may not take with you. They depend on where and with what kind of luggage you are travelling.

- Experiment:
 Here you can test your knowledge regarding: what is totally **forbidden** on board of a aircraft, what you **can (or should)** put in a **[checked-in luggage](glossary://checked-in%20luggage)**, what you **can** put in **cabin luggage** and what you should **[prepare](glossary://prepare) for safety screening**. Simply swipe an object to appropriate hole 😁


  Here you must specify how many objects you would like to test yourself with:
 */
let amountOfItems: Int = /*#-editable-code root node*/<#from 5 to 57#>/*#-end-editable-code*/
//#-hidden-code
sendValue(.data(try NSKeyedArchiver.archivedData(withRootObject: amountOfItems, requiringSecureCoding: true)))
//#-end-hidden-code
/*:
 - Note:
 Before you fly check you airline and destination country policy regarding what you can take with you! **It might be different**!
 */

